<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Student;
use App\Club;
use App\Http\Resources\StudentResource;
use Validator;

class StudentAPIController extends Controller
{
    //
    public function index()
	{
		$students = Student::orderBy('created_at', 'asc')->with(['club'])->get();
		// $club = Club::pluck('name', 'id');
		if($students)
		{
			return response()->json(['data' => StudentResource::collection($students)],200);
			// return StudentResource::collection($students);
		}
		else
		{
			return response()->json(['error'=>'No Such Data'], 401);
		}

	}

	public function store(Request $request)
	{
		$validator = Validator::make($request->all(), [ 
			'name'=>'required',
			'club_id'=>'required',
		]);

		if ($validator->fails()) { 
			return response()->json(['error'=>$validator->errors()], 401);            
		}

		$student = Student::create([
			'name' => $request->input('name'),
			'club_id' => $request->input('club_id'),
		]);
		return response()->json(['data' => new StudentResource($student)],200);
		// return new StudentResource($student);
	}

	public function show(Student $student)
	{
		$student = new StudentResource($student);
		if($student){
			return response()->json(['data' => $student],200);
		}else{
			return response()->json(['Error'=>'No Such Data'], 401);
		}
	}

	public function edit(Student $student)
    {
        $student = new StudentResource($student);
		if($student){
			return response()->json(['data' => $student],200);
		}else{
			return response()->json(['Error'=>'No Such Data'], 401);
		}
    }

	public function update(Request $request,$id)
	{
		$validator = Validator::make($request->all(), [ 
			'name'=>'required',
			'club_id'=>'required',
		]);

		if ($validator->fails()) { 
			return response()->json(['error'=>$validator->errors()], 401);            
		}

		$student = Student::find($id);
		$student->name = $request->input('name');
		$student->club_id = $request->input('club_id');
		$student->save();

		return response()->json(['data' => new StudentResource($student)],200);
		// return new StudentResource($student);
	}

	public function destroy(Student $student)
	{
		$student->delete();

		return response()->json(null, 204);
	}
}
