<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Club;
use App\Http\Resources\ClubResource;
use Validator;

class ClubAPIController extends Controller
{
    //
	public function index()
	{
		$clubs = Club::orderBy('created_at', 'asc')->get();
		
		if($clubs)
		{
			return response()->json(['data' => ClubResource::collection($clubs)],200);
			// return ClubResource::collection(Club::all());
		}
		else
		{
			return response()->json(['error'=>'No Such Data'], 401);
		}

	}

	public function store(Request $request)
	{
		$validator = Validator::make($request->all(), [ 
			'name'=>'required',
		]);

		if ($validator->fails()) { 
			return response()->json(['error'=>$validator->errors()], 401);            
		}

		$club = Club::create([
			'name' => $request->input('name'),
		]);

		return response()->json(['data' => new ClubResource($club)],200);
	}

	public function show(Club $club)
	{
		$club = new ClubResource($club);
		if($club){
			return response()->json(['data' => $club],200);
		}else{
			return response()->json(['Error'=>'No Such Data'], 401);
		}
		// return new ClubResource($club);
	}

	public function edit(Club $club)
    {
        $club = new ClubResource($club);
		if($club){
			return response()->json(['data' => $club],200);
		}else{
			return response()->json(['Error'=>'No Such Data'], 401);
		}

        // return new ClubResource($club);
    }

	public function update(Request $request, $id)
	{
		$validator = Validator::make($request->all(), [ 
			'name'=>'required',
		]);

		if ($validator->fails()) { 
			return response()->json(['error'=>$validator->errors()], 401);            
		}


		$club = Club::find($id);
		$club->name = $request->input('name');
		$club->save();
		return response()->json(['data' => new ClubResource($club)],200);
		// return new ClubResource($club);
	}

	public function destroy(Club $club)
	{
		$club->delete();

		return response()->json(null, 204);
	}
}
