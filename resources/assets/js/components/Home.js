import React, { Component } from 'react';
import axios from 'axios';
import{BrowserRouter as Router , Link , Route} from 'react-router-dom';

export default class Home extends Component {
    constructor(){
		super();
		this.state = {
			clubs:[],
			students:[],
		}
	}
	
	componentDidMount(){
		axios.get('http://localhost:8000/api/clubs').then(response=>{
			// means save the response data into the categories
			this.setState({clubs:response.data.data});
		
		});
		axios.get('http://localhost:8000/api/students').then(response=>{
			// means save the response data into the categories
			this.setState({students:response.data.data});
		
		});
	}

    render() {
        return (
            <div className="container">
                HOME PAGE
                <p><b>Club List</b></p>
                <table className="table">
		            <thead>
			            <tr>
				            <th scope="col">#</th>
				            <th scope="col">Club Name</th>
				            <th scope="col">Created At</th>
				            <th scope="col">Updated At</th>
				        </tr>
				    </thead>

				    <tbody>
				    {
				    	this.state.clubs.map(club=>{
				    		return (
				       		 <tr key={club.id}>
					            <th scope="row">{club.id}</th>
					            <td>{club.name}</td>
					            <td>{club.created_at.date}</td>
					            <td>{club.updated_at.date}</td>
			          		  </tr>
				    		)
				    	})
		            }
		            </tbody>
            	</table>
            	<p><b>Student List</b></p>
            	<table className="table">
		            <thead>
			            <tr>
				            <th scope="col">#</th>
				            <th scope="col">Student Name</th>
				            <th scope="col">Club ID</th>
				            <th scope="col">Created At</th>
				            <th scope="col">Updated At</th>
				        </tr>
				    </thead>

				    <tbody>
				    {
				    	this.state.students.map(student=>{
				    		return (
				       		 <tr key={student.id}>
					            <th scope="row">{student.id}</th>
					            <td>{student.name}</td>
					            <td>{student.club}</td>
					            <td>{student.created_at.date}</td>
					            <td>{student.updated_at.date}</td>
			          		  </tr>
				    		)
				    	})
		            }
		            </tbody>
            	</table>
            </div>
        );
    }
}
