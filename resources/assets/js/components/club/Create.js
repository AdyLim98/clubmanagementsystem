import React, { Component } from 'react';
import axios from 'axios';
import Success from './CreateSucess';
import Fail from './CreateFail';

export default class Create extends Component {
	constructor(){
		super();
		this.onchangeClub = this.onchangeClub.bind(this);
		this.onSubmit = this.onSubmit.bind(this);
		this.state={
			name:'',
			alert_message:'',
		}	
	}

	onchangeClub(e){
		this.setState({
			name:e.target.value
		});
	}

	onSubmit(e){
		// For Refresh
		e.preventDefault();
		const club = {
			name : this.state.name
		}
		// pass const category to the post api method
		axios.post('http://localhost:8000/api/clubs',club).then(
			response=>{
				this.setState({alert_message:"Success"})
			}).catch(error=>{
				this.setState({alert_message:"Fail"})
			});
	}

    render() {
        return (
            <div className="container">

            {this.state.alert_message == "Success" ? <Success /> : null}
            {this.state.alert_message == "Fail" ? <Fail /> : null}
	            
	            <form onSubmit={this.onSubmit}>
	            	<div className="form-group">
	            		<label>Club Name</label>
	            			<input type="text" 
	            			className="form-control" 
	            			value={this.state.name} 
	            			onChange={this.onchangeClub}
	            			/>
	            	</div>
	            
	            	<button type="submit" className="btn btn-primary">Submit</button>
	            </form>
            </div>
        );
    }
}


