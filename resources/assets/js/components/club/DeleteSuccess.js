import React, { Component } from 'react';

export default class DeleteSuccess extends Component {
    render() {
        return (
            <div className="alert alert-success" role="alert">
                Record Delete Successfully
            </div>
        );
    }
}


