import React, { Component } from 'react';
import axios from 'axios';
import{BrowserRouter as Router , Link , Route} from 'react-router-dom';

export default class Show extends Component {
	constructor(){
		super();
		this.state = {
			id:'',
			name:'',
			created_at:'',
			updated_at:'',
		}
	}
	
	componentDidMount(){
		axios.get('http://localhost:8000/api/clubs/'+this.props.match.params.id).then(response=>{
			// means save the response data into the categories
			this.setState({
				id:response.data.data.id,
				name:response.data.data.name,
				created_at:response.data.data.created_at.date,
				updated_at:response.data.data.updated_at.date,
			});
		});
	}

    render() {
        return (
            <div className="container">
         
            	<p><b>Club List</b></p>
            	<table className="table">
		            <thead>
			            <tr>
				            <th scope="col">#</th>
				            <th scope="col">Club Name</th>
				            <th scope="col">Created At</th>
				            <th scope="col">Updated At</th>
				        </tr>
				    </thead>

				    <tbody>
				    {
				        <tr key={this.state.id}>
				        	<td>{this.state.id}</td>
					        <td>{this.state.name}</td>
					        <td>{this.state.created_at}</td>
					        <td>{this.state.updated_at}</td>
			          	</tr>
		            }
		            </tbody>
            	</table>
            </div>
        );
    }
}
