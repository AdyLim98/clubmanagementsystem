import React, { Component } from 'react';
import axios from 'axios';
import Success from './Success';
import Fail from './Fail';
// use props here to get the URL parameter
export default class Edit extends Component {
	constructor(props){
		super(props);
		this.onchangeClub = this.onchangeClub.bind(this);
		this.onSubmit = this.onSubmit.bind(this);
		this.state={
			name:'',
			alert_message:'',
		}	
	}

	componentDidMount(){
		axios.get('http://localhost:8000/api/clubs/'+this.props.match.params.id).then(response=>{
			// means save the response data into the categories
			this.setState({name:response.data.data.name});
		});
	}

	onchangeClub(e){
		this.setState({
			name:e.target.value
		});
	}

	onSubmit(e){
		// For Refresh
		e.preventDefault();
		const club = {
			name : this.state.name
		}
		// pass const category to the post api method
		axios.put('http://localhost:8000/api/clubs/'+this.props.match.params.id,club).then(
			response=>{
				this.setState({alert_message:"Success"})
			}).catch(error=>{
				this.setState({alert_message:"Fail"})
			});
	}

    render() {
        return (
            <div className="container">
           
            {this.state.alert_message == "Success" ? <Success /> : null}
            {this.state.alert_message == "Fail" ? <Fail /> : null}

	            <form onSubmit={this.onSubmit}>
	            	<div className="form-group">
	            		<label>Club Name</label>
	            			<input type="text" 
	            			className="form-control" 
	            			value={this.state.name} 
	            			onChange={this.onchangeClub}
	            			/>
	            	</div>
	            
	            	<button type="submit" className="btn btn-primary">Submit</button>
	            </form>
            </div>
        );
    }
}


