import React, { Component } from 'react';
import axios from 'axios';
import{BrowserRouter as Router , Link , Route} from 'react-router-dom';
import Success from './DeleteSuccess';
import Fail from './DeleteFailed'; 

export default class Listing extends Component {
	constructor(){
		super();
		this.state = {
			clubs:[],
			alert_message:'',
		}
	}
	
	componentDidMount(){
		axios.get('http://localhost:8000/api/clubs').then(response=>{
			// means save the response data into the categories
			this.setState({clubs:response.data.data});
		
		});
	}
	onDelete(club_id)
	{
		axios.delete('http://localhost:8000/api/clubs/'+club_id).then(response=>{
			var clubs = this.state.clubs;
			for(var i = 0 ; i<clubs.length ; i++){
				if(clubs[i].id == club_id){
					clubs.splice(i,1);
					this.setState({clubs:clubs});
				}
			}
			this.setState({alert_message:"Success"})
		}).catch(error=>{
			this.setState({alert_message:"Fail"})
		});
	}
    render() {
        return (
            <div className="container">

            {this.state.alert_message == "Success" ? <Success /> : null}
            {this.state.alert_message == "Fail" ? <Fail /> : null}

                <p><b>Club List</b></p>
                <table className="table">
		            <thead>
			            <tr>
				            <th scope="col">#</th>
				            <th scope="col">Club Name</th>
				            <th scope="col">Created At</th>
				            <th scope="col">Updated At</th>
				            <th scope="col">Action</th>
				        </tr>
				    </thead>

				    <tbody>
				    {
				    	this.state.clubs.map(club=>{
				    		return (
				       		 <tr key={club.id}>
					            <th scope="row">{club.id}</th>
					            <td><Link to ={"/club/show/"+(club.id)}>{club.name}</Link></td>
					            <td>{club.created_at.date}</td>
					            <td>{club.updated_at.date}</td>
					            <td>
					             	<Link to={"/club/edit/"+(club.id)} className="btn btn-success">Edit</Link>
				            		<a href="#" onClick={this.onDelete.bind(this,club.id)} className="btn btn-danger" style={{marginLeft:"10px"}}>Delete</a>
			          		  	</td>
			          		  </tr>
				    		)
				    	})
		            }
		            </tbody>
            	</table>
            </div>
        );
    }
}
