import React, { Component } from 'react';

import {BrowserRouter as Router , Link ,Route} from 'react-router-dom';
import Home from './Home';
import Club from './club/index';
import Student from './student/index';

export default class Header extends Component {
    render() {
        return (
            <Router>
            <div className="container">
           
            <nav className="navbar navbar-expand-lg navbar-dark bg-dark">

                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>

                <div className="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul className="navbar-nav mr-auto">
                        <li className="nav-item">
                            <Link className="nav-link" to="/">Home </Link>
                        </li>
                        <li className="nav-item">
                            <Link className="nav-link" to="/club">Club</Link>
                        </li>
                        <li className="nav-item">
                            <Link className="nav-link" to="/student">Student</Link>
                        </li>
                    </ul>


                </div>
            </nav>

                <Route exact path='/' component={Home} />
                <Route exact path='/club' component={Club} />
                <Route exact path='/student' component={Student} />

            </div>
            </Router>
        );
    }
}


