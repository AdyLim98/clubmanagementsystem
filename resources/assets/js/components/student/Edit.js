import React, { Component } from 'react';
import axios from 'axios';
import Success from './Success';
import Fail from './Fail';

// use props here to get the URL parameter
export default class Edit extends Component {
	constructor(props){
		super(props);
		this.onchangeStudent = this.onchangeStudent.bind(this);
		this.handleChange = this.handleChange.bind(this);
		this.onSubmit = this.onSubmit.bind(this);
		this.state={
			name:'',
			club_id:'',
			clubs:[],
			alert_message:'',
		}	
	}

	componentDidMount(){
		axios.get('http://localhost:8000/api/clubs').then(response=>{
			// means save the response data into the categories
			this.setState({clubs:response.data.data});
		
		});
		axios.get('http://localhost:8000/api/students/'+this.props.match.params.id).then(response=>{
			// means save the response data into the categories
			this.setState({
				name:response.data.data.name,
				club_id:response.data.data.club_id,
			});
		});
	}

	onchangeStudent(e){
		this.setState({
			name:e.target.value,
		});
	}

	handleChange(e) {
		this.setState({
			club_id: e.target.value
		});
	}

	onSubmit(e){
		// For Refresh
		e.preventDefault();
		const student = {
			name : this.state.name,
			club_id : this.state.club_id,
		}
		// pass const category to the post api method
		axios.put('http://localhost:8000/api/students/'+this.props.match.params.id,student).then(
			response=>{
				this.setState({alert_message:"Success"})
			}).catch(error=>{
				this.setState({alert_message:"Fail"})
			});
	}

    render() {
        return (
            <div className="container">

            {this.state.alert_message == "Success" ? <Success /> : null}
            {this.state.alert_message == "Fail" ? <Fail /> : null}

	            <form onSubmit={this.onSubmit}>
	            	<div className="form-group">
	            		<label>Student Name</label>
	            			<input type="text" 
	            			className="form-control" 
	            			value={this.state.name} 
	            			onChange={this.onchangeStudent}
	            		    />
	            	</div>

	            	<div className="form-group">
	            		<label>Club</label>
		            		<select className="form-control form-control-sm" value={this.state.club_id} onChange={this.handleChange}>
	            		{
	            		this.state.clubs.map(club=>{
	            			return(
		            			<option value={club.id}>{club.name}</option>
	            		    )
	            		})
	            	}
		            		</select> 
	            		
	            	</div>
	            
	            	<button type="submit" className="btn btn-primary">Submit</button>
	            </form>
            </div>
        );
    }
}


