import React, { Component } from 'react';
import axios from 'axios';
import{BrowserRouter as Router , Link , Route} from 'react-router-dom';
import Success from './DeleteSuccess';
import Fail from './DeleteFailed'; 

export default class Listing extends Component {
	constructor(){
		super();
		this.state = {
			students:[],
			alert_message:'',
		}
	}
	
	componentDidMount(){
		axios.get('http://localhost:8000/api/students').then(response=>{
			// means save the response data into the categories
			this.setState({students:response.data.data});
		
		});
	}

	onDelete(student_id)
	{
		axios.delete('http://localhost:8000/api/students/'+student_id).then(response=>{
			var students = this.state.students;
			for(var i = 0 ; i<students.length ; i++){
				if(students[i].id == student_id){
					students.splice(i,1);
					this.setState({students:students});
				}
			}
			this.setState({alert_message:"Success"})
		}).catch(error=>{
			this.setState({alert_message:"Fail"})
		});
	}

    render() {
        return (
            <div className="container">

            {this.state.alert_message == "Success" ? <Success /> : null}
            {this.state.alert_message == "Fail" ? <Fail /> : null}
         
            	<p><b>Student List</b></p>
            	<table className="table">
		            <thead>
			            <tr>
				            <th scope="col">#</th>
				            <th scope="col">Student Name</th>
				            <th scope="col">Club ID</th>
				            <th scope="col">Created At</th>
				            <th scope="col">Updated At</th>
				            <th scope="col">Action</th>
				        </tr>
				    </thead>

				    <tbody>
				    {
				    	this.state.students.map(student=>{
				    		return (
				       		 <tr key={student.id}>
					            <th scope="row">{student.id}</th>
					            <td><Link to ={"/student/show/"+(student.id)}>{student.name}</Link></td>
					            <td>{student.club}</td>
					            <td>{student.created_at.date}</td>
					            <td>{student.updated_at.date}</td>
					            <td>
					             	<Link to={"/student/edit/"+(student.id)} className="btn btn-success">Edit</Link>
				            		<a href="#" onClick={this.onDelete.bind(this,student.id)} className="btn btn-danger" style={{marginLeft:"10px"}}>Delete</a>
			          		  	</td>
			          		  </tr>
				    		)
				    	})
		            }
		            </tbody>
            	</table>
            </div>
        );
    }
}
