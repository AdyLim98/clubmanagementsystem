import React, { Component } from 'react';

export default class Fail extends Component {
    render() {
        return (
            <div className="alert alert-danger" role="alert">
                Failed To Update
            </div>
        );
    }
}


